import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AuthGuardService } from './auth/auth-guard.service';
import { AuthService } from './auth/auth.service';
import { AppComponent } from './app.component';
import { JwtModule } from '@auth0/angular-jwt'
import { SigninComponent } from './signin/signin.component';
import {
  SocialLoginModule,
  AuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider,
} from "angular5-social-login";
import { AppRoutingModule } from './/app-routing.module';
import { SuccessComponent } from './success/success.component';
// Configs 
export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
    [
      {
        id: FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider("1652774851467642")
      },
      {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider("494288397962-b4neot39ag0vdtjjp510uuk6usk6t8fr.apps.googleusercontent.com")
      }
    ]);
  
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    SuccessComponent
  ],
  imports: [
    BrowserModule,
    SocialLoginModule,
    AppRoutingModule,
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: () => {
          return localStorage.getItem('token');
        }
      }
    })
  ],
  providers: [{
    provide: AuthServiceConfig,
    useFactory: getAuthServiceConfigs
  },
    { provide: 'LOCALSTORAGE', useFactory: getLocalStorage },
    AuthGuardService, AuthService
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
export function getLocalStorage() {
  return (typeof window !== "undefined") ? window.localStorage : null;
}
