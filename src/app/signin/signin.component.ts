import { Component, OnInit, PLATFORM_ID, Injectable, Inject } from '@angular/core';
import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider
} from 'angular5-social-login';
import { Router } from '@angular/router';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { AuthService as AuthToken } from '../auth/auth.service';
import { sign } from 'jsonwebtoken';


@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})

@Injectable()
export class SigninComponent implements OnInit {

  constructor(private socialAuthService: AuthService, private router: Router,
    @Inject(PLATFORM_ID) private platformId: any,
    @Inject('LOCALSTORAGE') private localStorage: any) { }

  ngOnInit() {
  }
  public socialSignIn(socialPlatform: string) {
    let socialPlatformProvider;
    if (socialPlatform == "facebook") {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialPlatform == "google") {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }

    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        debugger;
        this.authenticateUser();
        this.router.navigateByUrl("/success");
      }
    );
  }
  authenticateUser(): any {
    var token = sign({ exp: new Date(2019, 0, 1, 0, 0, 0).getTime() }, 'shhhhh');
    localStorage.setItem("token", token);
    console.log(token);
  }

}
